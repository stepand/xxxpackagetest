This is a test of Go Module Mirror.

The original name of this repo was `stepand/packagetest` and this is how the module was cached by module mirror (see https://pkg.go.dev/mod/bitbucket.org/stepand/packagetest).

The repository has been renamed (same like deleted), so it's not possile to reach the module again, it's fetched from the mirror cache. Let's see for how long it remains in the cache...